#include<stdio.h>
#include<math.h>

float
get_radius ()
{
  float radius;
  printf ("enter radius\n");
  scanf ("%f", &radius);
  return radius;
}

float
compute_area (float radius)
{
  float area;
  area = 3.14 * radius * radius;
  return area;
}

float
compute_circumference (float radius)
{
  float circumference;
  circumference = 2 * 3.14 * radius;
  return circumference;
}

void
output (float area, float circumference)
{
  printf ("the area and circumference of the circle is %f\t %f\n", area,
	  circumference);
}

int
main ()
{
  float radius, area, circumference;
  radius = get_radius ();
  area = compute_area (radius);
  circumference = compute_circumference (radius);
  output (area, circumference);
  return 0;
}